package com.group5.signlanguagelearning.learn;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.google.common.base.Charsets;
import com.google.common.io.ByteStreams;
import com.google.common.io.Resources;
import com.group5.signlanguagelearning.NavigationActivity;
import com.group5.signlanguagelearning.R;
import com.group5.signlanguagelearning.data.DataDummySource;
import com.group5.signlanguagelearning.dict.DictionaryFragment;
import com.group5.signlanguagelearning.dict.DictionaryPresenter;
import com.group5.signlanguagelearning.util.ActivityUtils;

import java.io.IOException;
import java.io.InputStream;


public class LearnActivity extends NavigationActivity {
    LearnContract.Presenter mLearnPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar toolbar = getSupportActionBar();
        toolbar.setTitle(R.string.title_learning);

        // set up the fragment
        LearnFragment learnFragment =
                (LearnFragment) getFragmentManager().findFragmentById(R.id.contentFrame);

        if (learnFragment == null){
            learnFragment = new LearnFragment();
            ActivityUtils.addFragmentToActivity(getFragmentManager(), learnFragment,
                    R.id.contentFrame);
        }

        // presenter gets reference to view
        mLearnPresenter = new LearnPresenter(learnFragment, DataDummySource.getInstance(getApplicationContext()));
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_learn;
    }

    @Override
    public int getNavigationMenuItemId() {
        return R.id.navigation_learning;
    }

    @Override
    public int getActionBarId() {
        return R.id.toolbar;
    }


}
