package com.group5.signlanguagelearning.learn.learnSequence;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.group5.signlanguagelearning.BuildConfig;
import com.group5.signlanguagelearning.data.DataDummySource;
import com.group5.signlanguagelearning.data.Lesson;
import com.group5.signlanguagelearning.data.MultipleChoiceUnit;
import com.group5.signlanguagelearning.data.Video;
import com.group5.signlanguagelearning.data.VideoProvider;
import com.group5.signlanguagelearning.data.VocabWord;
import com.group5.signlanguagelearning.dict.vocabPresentation.VocabPresentationContract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by juwinkler on 28.04.18.
 */

public class LearnSequencePresenter implements LearnSequenceContract.Presenter{
    private VideoProvider provider;
    private VocabWord correctWordToShow;
    private LearnSequenceContract.View mPresView;
    private Context context;
    private int currentUnitIndex;
    private List<MultipleChoiceUnit> multipleChoiceUnits;
    private MultipleChoiceUnit currentUnitToShow;
    private Lesson lessonToShow;
    private DataDummySource d;
    private Boolean isFeedbackShown; // A boolean to keep track of whether or not feedback is currently shown

    // Need DataDummySource AND Provider??
    public LearnSequencePresenter(VideoProvider provider,  LearnSequenceContract.View mPresView, Context context, Lesson lesson) {
        this.provider = provider;
        this.mPresView = mPresView;
        this.context = context;
        this.lessonToShow = lesson;
        this.multipleChoiceUnits = this.lessonToShow.getmMultipleChoiceUnits();
        this.currentUnitIndex = 0; // start at unit 0
        this.currentUnitToShow = multipleChoiceUnits.get(currentUnitIndex); //Start with unit 0
        this.d = DataDummySource.getInstance(context);
        this.correctWordToShow = d.getVocabWordForID(currentUnitToShow.getCorrectWordID());
        showCurrentMultipleChoiceUnit();
    }

    @Override
    public void start() {
        this.openUpVideo();
    }


    @Override
    public void setLesson() {

    }

    @Override
    public void openUpVideo() {
        this.mPresView.showWord(correctWordToShow.getWord());
        Video v = provider.getVideoForId(correctWordToShow.getVideoId());
        String packageName = BuildConfig.APPLICATION_ID;
        // get the resource id: TODO generalize this
        int id = context.getResources().getIdentifier(v.getVideoPath(),"raw",packageName);
        String UriPath = "android.resource://" + packageName + "/"
                + id;

        Uri videoUri = Uri.parse(UriPath);
        this.mPresView.showVideo(videoUri);
    }


    public void showCurrentMultipleChoiceUnit(){
        // Get words for IDs:
        List<String> wordsToShow = new ArrayList<>();
        for(String currentWordID: this.currentUnitToShow.getAllWordIDs()){
            wordsToShow.add(d.getVocabWordForID(currentWordID).getWord());
        }
        // Randomize order of wordsToShow:
        Collections.shuffle(wordsToShow);
        // Update the new correct word:
        this.correctWordToShow = d.getVocabWordForID(this.currentUnitToShow.getCorrectWordID());
        // Call methods in view (-> update RadioGroup and Video):
        mPresView.showMultipleChoiceWords(wordsToShow);
        openUpVideo();
        // First, hide the continue-button
        mPresView.changeContinueButtonVisibility();
    }

    @Override
    public void update(){
        // First, assure that we are notat the end of all multiple choice units:
        if (currentUnitIndex<multipleChoiceUnits.size()-1){
            // Count up
            this.currentUnitIndex +=1;
            this.currentUnitToShow = multipleChoiceUnits.get(currentUnitIndex);
            showCurrentMultipleChoiceUnit();
            //handleFeedback();
        }
        // If we are at the end, close the activity
        else {
            mPresView.close();
        }
    }

    @Override
    public void provideFeedback(){
        // Feedback: Highlight buttons in correct colors
        mPresView.highlightButtons(d.getVocabWordForID(currentUnitToShow.getCorrectWordID()).getWord());
        // Also, show the continue-button again
        mPresView.changeContinueButtonVisibility();
    }

}
