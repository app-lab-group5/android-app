package com.group5.signlanguagelearning.learn;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.SearchView;

import com.group5.signlanguagelearning.R;
import com.group5.signlanguagelearning.data.Category;
import com.group5.signlanguagelearning.data.Lesson;
import com.group5.signlanguagelearning.dict.DictionaryFragment;
import com.group5.signlanguagelearning.learn.learnSequence.LearnSequenceActivity;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class LearnFragment extends Fragment implements LearnContract.View {
    LearnContract.Presenter mLearnPresenter;
    LessonAdapter mLessonAdapter;

    @Override
    public void setPresenter(LearnContract.Presenter presenter) {
        this.mLearnPresenter = presenter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLessonAdapter = new LessonAdapter(
                new ArrayList<Lesson>(0),mLessonListener);
    }

    @Nullable
    @Override
    // called when this fragment is supposed to render its view
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.learn_fragment, container, false);

        GridView categoryGrid = (GridView) root.findViewById(R.id.lessonGrid);
        // make this capable of updating its contents based on the adapter
        categoryGrid.setAdapter(mLessonAdapter);

        return root;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        // TODO find out why this null check is necessary and if the reason
        // might cause us problems later down the road
        if(mLearnPresenter != null) {
            mLearnPresenter.start();
        }
    }

    @Override
    public void showLessonActivity(Lesson lesson) {
        Intent i = new Intent(getActivity().getApplicationContext(),LearnSequenceActivity.class);
        // tell the activity which lesson we want to show
        i.putExtra(LearnSequenceActivity.LESSON_ID,lesson.getId());
        // start the activity
        startActivity(i);
    }

    @Override
    public void showLessons(List<Lesson> lessonList) {
        mLessonAdapter.setList(lessonList);
    }

    private static class LessonAdapter extends BaseAdapter {

        private List<Lesson> mLessons;
        private LessonItemListener mLessonItemListener;

        public LessonAdapter(List<Lesson> lessons, LessonItemListener mLessonItemListener) {
            setList(lessons);
            this.mLessonItemListener = mLessonItemListener;
        }

        private void setList(List<Lesson> lessons){
            mLessons = checkNotNull(lessons);}

        @Override
        public int getCount() {
            return mLessons.size();
        }

        @Override
        public Lesson getItem(int position) {
            return mLessons.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        // how to render individuals lessons as views
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View gridItem = convertView;
            // if this view has not been drawn yet,
            // build a new one from the XML layout spec
            if(gridItem == null){
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                gridItem = inflater.inflate(R.layout.lesson_item, parent, false);
            }
            // the category we want to put into this view
            final Lesson lesson = getItem(position);

            // this is where the XML definition of button is extracted
            Button categoryButton = (Button) gridItem.findViewById(R.id.category_button);
            // set the text
            categoryButton.setText(lesson.getName());
            // set the drawable to the top location
            categoryButton.setCompoundDrawables(null,lesson.getIcon(),null,null);

            // make this thing clickable
            categoryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLessonItemListener.onLessonClick(lesson);
                }
            });

            return gridItem;
        }
    }
    // interface for listening to clicks on lesson items
    public interface LessonItemListener {
        void onLessonClick(Lesson clickedLesson);
    }

    // what to do when a lesson item is clicked in the grid
    LessonItemListener mLessonListener = new LessonItemListener() {
        @Override
        public void onLessonClick(Lesson clickedLesson) {
            // call a method in the presenter for opening up the lesson that was clicked
            mLearnPresenter.openUpLesson(clickedLesson);
        }
    };
}
