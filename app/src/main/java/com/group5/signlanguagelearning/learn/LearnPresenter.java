package com.group5.signlanguagelearning.learn;

import com.group5.signlanguagelearning.community.CommunityContract;
import com.group5.signlanguagelearning.data.Lesson;
import com.group5.signlanguagelearning.data.LessonProvider;

import java.util.List;

public class LearnPresenter implements LearnContract.Presenter {
    private final LearnContract.View mLearnView;
    private LessonProvider mLessonProvider;

    public LearnPresenter(LearnContract.View mLearnView,LessonProvider mLessonProvider) {
        this.mLearnView = mLearnView;
        mLearnView.setPresenter(this);
        this.mLessonProvider = mLessonProvider;
    }


    @Override
    public void start() {
        loadLessons();
    }

    private void loadLessons(){
        List<Lesson> mLessonList = mLessonProvider.getLessons();
        mLearnView.showLessons(mLessonList);
    }

    @Override
    public void openUpLesson(Lesson lesson) {
        mLearnView.showLessonActivity(lesson);
    }
}
