package com.group5.signlanguagelearning.learn.learnSequence;

import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.VideoView;

import com.group5.signlanguagelearning.R;
import com.group5.signlanguagelearning.data.DataDummySource;
import com.group5.signlanguagelearning.data.Lesson;
import com.group5.signlanguagelearning.data.MultipleChoiceUnit;

import java.util.List;

public class LearnSequenceActivity extends AppCompatActivity implements LearnSequenceContract.View{

    private TextView mTextView;
    private VideoView mVideoView;
    private RadioGroup mRadioGroup;
    private Button mContinueButton;
    private LearnSequenceContract.Presenter mPresenter;
    private boolean feedbackProvided; // A boolean to check whether feedback has already been shown.
    public static String LESSON_ID = "lesson_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn_sequence);
        DataDummySource d = DataDummySource.getInstance(this.getApplicationContext());
        // retrieve which lesson we wanted to show...
        String lessonId = getIntent().getStringExtra(LESSON_ID);
        //retrieve the lesson and depict it
        Lesson toShow = d.getLessonForID(lessonId);
        // get components of window
        mTextView = (TextView) findViewById(R.id.videoLearning_videoName); // only needed to check whether word is correct -> debug
        mTextView.setVisibility(View.INVISIBLE);
        mVideoView = (VideoView) findViewById(R.id.videoLearning_videoView);
        mRadioGroup = (RadioGroup) findViewById(R.id.radioButtonOptions);
        mContinueButton = (Button) findViewById(R.id.continue_Button);
        MediaController controller = new MediaController(this);
        controller.setAnchorView(mVideoView);
        mVideoView.setMediaController(controller);

        // Get all multiple choice units
        //final List<MultipleChoiceUnit> multipleChoiceUnits = toShow.getmMultipleChoiceUnits();
        //this.currentUnitIndex = 0; // start with 0, count up when button is clicked
        //this.currentUnitToShow = multipleChoiceUnits.get(currentUnitIndex); //Start with unit 0
        //mPresenter = new LearnSequencePresenter(d,d.getVocabWordForID(currentUnitToShow.getCorrectWordID()), this, this.getApplicationContext(),toShow);

        mPresenter = new LearnSequencePresenter(d, this, this.getApplicationContext(),toShow);

        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Ask: Feedback already given? If not -> count up
                mPresenter.update();
                //currentUnitIndex += 1;
                //currentUnitToShow = multipleChoiceUnits.get(currentUnitIndex);
                // call update
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // TODO find out why this null check is necessary
        if(mPresenter != null) {
            mPresenter.start();
        }
    }

    @Override
    public void showVideo(Uri videoUri) {
        mVideoView.setVideoURI(videoUri);
        mVideoView.requestFocus();
        mVideoView.start();
    }

    @Override
    public void showWord(String wordToShow) {
        mTextView.setText(wordToShow);
    }

    @Override
    public void setPresenter(LearnSequenceContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showMultipleChoiceWords(List<String> multipleChoiceOptions){
        // First, empty radio Group
        mRadioGroup.removeAllViews();
        // Then, fill it again
        for (String wordOption: multipleChoiceOptions){
            RadioButton r = new RadioButton(this.getApplicationContext());
            r.setText(wordOption);
            r.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPresenter.provideFeedback();
                }
            });
            mRadioGroup.addView(r);
        }
        // Disable text view by default
        mTextView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void close(){
        finish();
    }

    @Override
    public void highlightButtons(String correctWord){
        for (int i = 0; i < mRadioGroup.getChildCount(); i++) {
            RadioButton currentButton = (RadioButton) mRadioGroup.getChildAt(i);
            // Highlight the button in the correct color:
            if (currentButton.getText()==correctWord){
                currentButton.setBackgroundColor(Color.parseColor("#d9ffcc"));
            }
            else{
                currentButton.setBackgroundColor(Color.parseColor("#ffe6e6"));
            }
            currentButton.setEnabled(false);
        }
        mTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void changeContinueButtonVisibility(){
        mContinueButton.setEnabled(!mContinueButton.isEnabled());

    }
}
