package com.group5.signlanguagelearning.learn.learnSequence;

import android.net.Uri;

import com.group5.signlanguagelearning.BasePresenter;
import com.group5.signlanguagelearning.BaseView;

import java.util.List;

public class LearnSequenceContract {

    interface View extends BaseView<Presenter>{
        public void showVideo(Uri videoUri);
        public void showWord(String wordToShow);
        public void showMultipleChoiceWords(List<String> multipleChoiceOptions);
        public void close();
        public void highlightButtons(String correctWord);
        public void changeContinueButtonVisibility();
    }
    interface Presenter extends BasePresenter{
        public void setLesson();
        public void openUpVideo();
        public void update();
        public void provideFeedback();
    }
}
