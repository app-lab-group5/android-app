package com.group5.signlanguagelearning.learn;

import android.support.annotation.NonNull;

import com.group5.signlanguagelearning.BasePresenter;
import com.group5.signlanguagelearning.BaseView;
import com.group5.signlanguagelearning.data.Lesson;

import java.util.List;

public class LearnContract {
    interface View extends BaseView<LearnContract.Presenter> {
        void showLessonActivity(Lesson lesson);
        void showLessons(List<Lesson> lessonList);
    }

    interface Presenter extends BasePresenter {
        void openUpLesson(Lesson lesson);
    }
}