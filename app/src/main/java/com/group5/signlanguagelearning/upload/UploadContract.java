package com.group5.signlanguagelearning.upload;

import com.group5.signlanguagelearning.BasePresenter;
import com.group5.signlanguagelearning.BaseView;

public class UploadContract {
    interface View extends BaseView<UploadContract.Presenter> {

    }

    interface Presenter extends BasePresenter {

    }
}
