package com.group5.signlanguagelearning.upload;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.group5.signlanguagelearning.R;

public class UploadFragment extends Fragment implements UploadContract.View {
    UploadContract.Presenter mUploadPresenter;

    @Override
    public void setPresenter(UploadContract.Presenter presenter) {
        this.mUploadPresenter = presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.upld_fragment, container, false);
        // TODO add fragment logic
        return root;
    }
}
