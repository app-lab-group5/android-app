package com.group5.signlanguagelearning.upload;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.group5.signlanguagelearning.NavigationActivity;
import com.group5.signlanguagelearning.R;
import com.group5.signlanguagelearning.dict.DictionaryFragment;
import com.group5.signlanguagelearning.dict.DictionaryPresenter;
import com.group5.signlanguagelearning.util.ActivityUtils;

public class UploadActivity extends NavigationActivity {
    UploadContract.Presenter mUploadPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar toolbar = getSupportActionBar();
        toolbar.setTitle(R.string.title_upload);

        // set up the fragment
        UploadFragment uploadFragment =
                (UploadFragment) getFragmentManager().findFragmentById(R.id.contentFrame);

        if (uploadFragment == null){
            uploadFragment = new UploadFragment();
            ActivityUtils.addFragmentToActivity(getFragmentManager(), uploadFragment,
                    R.id.contentFrame);
        }

        // presenter gets reference to view
        mUploadPresenter = new UploadPresenter(uploadFragment);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_upload;
    }

    @Override
    public int getNavigationMenuItemId() {
        return R.id.navigation_upload;
    }

    @Override
    public int getActionBarId() {
        return R.id.toolbar;
    }
}
