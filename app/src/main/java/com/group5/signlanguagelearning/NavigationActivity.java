package com.group5.signlanguagelearning;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.group5.signlanguagelearning.community.CommunityActivity;
import com.group5.signlanguagelearning.learn.LearnActivity;
import com.group5.signlanguagelearning.dict.DictionaryActivity;
import com.group5.signlanguagelearning.upload.UploadActivity;
// see: https://github.com/ddekanski/BottomNavigationViewBetweenActivities/blob/master/app/src/main/java/mobi/garden/bottomnavigationtest/BaseActivity.java

public abstract class NavigationActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener{

    protected BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentViewId());

        navigationView = (BottomNavigationView) findViewById(R.id.navigation);
        navigationView.setOnNavigationItemSelectedListener(this);

        // set up the toolbar, sub-class activities can obtain it via getSupportActionBar

        Toolbar toolbar = (Toolbar) findViewById(getActionBarId());
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateNavigationBarState();
    }

    // Remove inter-activity transition to avoid screen tossing on tapping bottom navigation items
    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        final int itemId = item.getItemId();
        final NavigationActivity activity = this;
        Runnable r = new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent();
                if (itemId == R.id.navigation_learning) {
                    i = new Intent(activity,LearnActivity.class);
                } else if (itemId == R.id.navigation_dictionary) {
                    i = new Intent(activity,DictionaryActivity.class);
                } else if (itemId == R.id.navigation_community) {
                    i = new Intent(activity,CommunityActivity.class);
                } else if (itemId == R.id.navigation_upload) {
                    i = new Intent(activity,UploadActivity.class);
                }
          //      i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);
                finish();
            }
        };
        navigationView.postDelayed(r, 300);
        return true;
    }

    private void updateNavigationBarState(){
        int actionId = getNavigationMenuItemId();
        selectBottomNavigationBarItem(actionId);
    }

    void selectBottomNavigationBarItem(int itemId) {
        MenuItem item = navigationView.getMenu().findItem(itemId);
        item.setChecked(true);
    }

    public abstract int getContentViewId();

    public abstract int getNavigationMenuItemId();

    public abstract int getActionBarId();
}
