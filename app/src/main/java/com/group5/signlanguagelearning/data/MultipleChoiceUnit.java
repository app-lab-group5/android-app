package com.group5.signlanguagelearning.data;

import java.util.List;

/**
 * Created by juwinkler on 28.04.18.
 */

// A class for multiple choice units: 5 words with 1 being considered the "correct" one
public class MultipleChoiceUnit {
    private final String mCorrectWordID; // The correct word
    private final List<String> mWrongWordIDs; // The wrong word
    private final List<String> mAllWordIDs; // All words together

    public MultipleChoiceUnit(String correctWordID, List<String> wrongWordIDs){
        this.mCorrectWordID = correctWordID;
        this.mWrongWordIDs = wrongWordIDs;
        wrongWordIDs.add(correctWordID);
        this.mAllWordIDs = wrongWordIDs;
    }

    public String getCorrectWordID(){
        return mCorrectWordID;
    }

    public List<String> getWrongWordIDs(){
        return mWrongWordIDs;
    }

    public List<String> getAllWordIDs(){
        return mAllWordIDs;
    }



}
