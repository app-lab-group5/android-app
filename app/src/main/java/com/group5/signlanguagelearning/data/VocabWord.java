package com.group5.signlanguagelearning.data;

import android.graphics.drawable.Drawable;

import java.util.List;
import java.util.UUID;

// TODO think about metadata we want to include her
public class VocabWord {

    private final String mId;
    // TODO think early about different languages...
    private final String mWordString;

    private final String mVideoId;

    public VocabWord(String wordString){
        this(UUID.randomUUID().toString(),wordString,UUID.randomUUID().toString());
    }

    public VocabWord(String wordString, Video v){
        this(UUID.randomUUID().toString(),wordString, v.getId());
    }

    public VocabWord(String mId, String mWordString, String mVideoId) {
        this.mId = mId;
        this.mWordString = mWordString;
        this.mVideoId = mVideoId;
    }

    public String getWord() {
        return mWordString;
    }

    public String getId() {
        return mId;
    }

    public String getVideoId() {
        return mVideoId;
    }
}
