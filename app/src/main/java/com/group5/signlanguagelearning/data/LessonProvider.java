package com.group5.signlanguagelearning.data;

import java.util.List;

public interface LessonProvider {
    List<Lesson> getLessons();

    Lesson getLessonForID(String id);
}
