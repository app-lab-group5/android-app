package com.group5.signlanguagelearning.data;

import java.util.List;

public interface VocabWordProvider {
    VocabWord getVocabWordForID(String id);
    List<VocabWord> getVocabWordsForID(List<String> ids);
    List<String> getVocabWordIdsForWord(String word);
}
