package com.group5.signlanguagelearning.data;

import android.graphics.drawable.Drawable;

import java.util.List;
import java.util.UUID;

// a class representing a particular set of words connected by category membership
public final class Category {

    // TODO look into ID system
    private final String mId;

    private final String mName;

    private final Drawable mIcon;

    private final List<String> mIncludedWordIds;

    public Category(String name, Drawable icon, List<String> wordIds){
        this(UUID.randomUUID().toString(),name,icon,wordIds);
    }

    public Category(String id, String name, Drawable icon, List<String> includedWordIds) {
        this.mId = id;
        this.mName = name;
        this.mIcon = icon;
        this.mIncludedWordIds = includedWordIds;
    }

    public Drawable getIcon() {
        return mIcon;
    }

    public List<String> getIncludedWordIds() {
        return mIncludedWordIds;
    }

    public String getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }
}
