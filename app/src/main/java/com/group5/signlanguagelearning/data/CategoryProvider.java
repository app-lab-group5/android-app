package com.group5.signlanguagelearning.data;

import java.util.List;

public interface CategoryProvider {

    List<Category> getCategories();
}
