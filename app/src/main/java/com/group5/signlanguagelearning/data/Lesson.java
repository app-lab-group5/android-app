package com.group5.signlanguagelearning.data;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

public class Lesson {
    // TODO look into ID system
    private final String mId;
    private final String mName;
    private final Drawable mIcon;
    private final List<String> mIncludedWordIds;
    private final List<MultipleChoiceUnit> mMultipleChoiceUnits;

    public Lesson(String name, Drawable icon, List<String> wordIds){
        this(UUID.randomUUID().toString(),name,icon,wordIds);
    }

    public Lesson(String id, String name, Drawable icon, List<String> includedWordIds) {
        this.mId = id;
        this.mName = name;
        this.mIcon = icon;
        this.mIncludedWordIds = includedWordIds;
        this.mMultipleChoiceUnits = createMultipleChoiceUnits();
    }

    // A function that creates a List of multiple choice units for the given lesson
    public List<MultipleChoiceUnit> createMultipleChoiceUnits(){
        List<MultipleChoiceUnit> multipleChoiceUnits = new ArrayList<MultipleChoiceUnit>();
        for (String word: mIncludedWordIds){
            // Create a new MultipleChoiceUnit with the current word as correct word
            multipleChoiceUnits.add(new MultipleChoiceUnit(word, getWrongWords(mIncludedWordIds, word, 4)));
        }
        return multipleChoiceUnits;
    }

    // A function that randomly chooses 4 "wrong words" from a list
    // https://stackoverflow.com/questions/8378752/pick-multiple-random-elements-from-a-list-in-java
    private  List<String> getWrongWords(List<String> allWords, String correctWord, int n) {
        List<String> wrongWords = new ArrayList<String>(allWords);
        wrongWords.remove(correctWord);
        Collections.shuffle(wrongWords);
        n = n < wrongWords.size() ? n : wrongWords.size();
        return wrongWords.subList(0, n);
    }

    public Drawable getIcon() {
        return mIcon;
    }

    public List<String> getIncludedWordIds() {
        return mIncludedWordIds;
    }

    public String getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public List<MultipleChoiceUnit> getmMultipleChoiceUnits(){ return mMultipleChoiceUnits;}
}
