package com.group5.signlanguagelearning.data;

public interface VideoProvider {
    Video getVideoForId(String id);
}
