package com.group5.signlanguagelearning.data;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.common.io.ByteStreams;
import com.group5.signlanguagelearning.R;
import com.group5.signlanguagelearning.util.GeneralUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.xdrop.fuzzywuzzy.FuzzySearch;
import me.xdrop.fuzzywuzzy.model.ExtractedResult;

public class DataDummySource implements VocabWordProvider, CategoryProvider, VideoProvider, LessonProvider{
    private List<Category> categories;
    private Map<String, VocabWord> wordMapping;
    private Map<String, Video> videoMapping;
    private Map<String, Lesson> lessonMapping;

    private static DataDummySource instance;

    private DataDummySource(Context context) {

        String content = "";

        InputStream is = context.getResources().openRawResource(R.raw.debug_words);

        try {
            content = new String(ByteStreams.toByteArray(is));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<String, List<String>> catMapping = new HashMap<>();
        videoMapping = new HashMap<>();
        wordMapping = new HashMap<>();
        lessonMapping = new HashMap<>();


        try {
            JSONObject jsonObject = new JSONObject(content);

            JSONArray jArray = jsonObject.getJSONArray("vocab_words");
            // read out words from json
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject vocabWords = jArray.getJSONObject(i);
                String name = vocabWords.getString("word");
                //NB using the resource name instead of the full path here, fix this later! fixme
                String video_name = vocabWords.getString("video_name");
                Video v = new Video(video_name);
                videoMapping.put(v.getId(), v);
                // hand the word a reference to the video id
                VocabWord word = new VocabWord(name, v);

                String category = vocabWords.getString("category");
                if (!catMapping.containsKey(category)) {
                    List<String> temp = new ArrayList<>();
                    temp.add(word.getId());
                    catMapping.put(category, temp);
                } else {
                    List<String> temp = catMapping.get(category);
                    temp.add(word.getId());
                    catMapping.put(category, temp);
                }
                wordMapping.put(word.getId(), word);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        categories = new ArrayList<>();
        // read in the categories
        for (String s : catMapping.keySet()) {
            Category c = new Category(s, null, catMapping.get(s));
            categories.add(c);
        }


        // read in the lessons
        for (Category c : categories) {
            Lesson l = new Lesson(c.getName(), c.getIcon(), c.getIncludedWordIds());
            // Mapping: IDs -> Lesson
            lessonMapping.put(l.getId(),l);
        }

    }

    private List<String> extractIDs(List<VocabWord> words) {
        List<String> strings = new ArrayList<>();
        for (VocabWord w : words) {
            strings.add(w.getId());
        }
        return strings;
    }

    private Map<String, VocabWord> getMapping(List<List<VocabWord>> listList) {
        Map<String, VocabWord> map = new HashMap<>();
        for (List<VocabWord> l : listList) {
            for (VocabWord w : l) {
                map.put(w.getId(), w);
            }
        }
        return map;
    }

    // TODO think of a better way to access a resource that doesn't involve passing the context all the time
    public static DataDummySource getInstance(Context context) {
        if (instance == null) {
            instance = new DataDummySource(context);
        }
        return instance;
    }

    @Override
    public VocabWord getVocabWordForID(String id) {
        return wordMapping.get(id);
    }

    @Override
    public List<VocabWord> getVocabWordsForID(List<String> ids) {
        List<VocabWord> words = new ArrayList<>();
        for (String id : ids) {
            words.add(getVocabWordForID(id));
        }
        return words;
    }

    @Override
    public List<String> getVocabWordIdsForWord(String word) {
        // TODO look at more efficient search, don't extract the words every time
        // we do this
        Collection<VocabWord> words = wordMapping.values();
        Map<String, String> toMatch = new HashMap<>();
        // store words and ids
        for (VocabWord item : words) {
            toMatch.put(item.getWord(), item.getId());
        }
        // TODO: score cutoff of 40, evaluate this
        List<ExtractedResult> res = FuzzySearch.extractSorted(word, toMatch.keySet(), 40);
        List<String> matched = new ArrayList<>();
        // retrieve word ids
        for (ExtractedResult r : res) {
            matched.add(toMatch.get(r.getString()));
        }
        return matched;
    }

    @Override
    public List<Category> getCategories() {
        return this.categories;
    }

    @Override
    public Video getVideoForId(String id) {
        return this.videoMapping.get(id);
    }

    @Override
    public List<Lesson> getLessons() {
        return new ArrayList<Lesson>(lessonMapping.values());
    }

    @Override
    public Lesson getLessonForID(String id) {
        return lessonMapping.get(id);
    }


}
