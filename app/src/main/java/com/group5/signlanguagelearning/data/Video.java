package com.group5.signlanguagelearning.data;

import java.util.UUID;

public class Video {
    private String mVideoId;
    private String mVideoPath;

    public Video(String videoPath) {
        this(UUID.randomUUID().toString(), videoPath);
    }

    public Video(String mVideoId, String videoPath) {
        this.mVideoId = mVideoId;
        this.mVideoPath = videoPath;
    }

    public String getVideoPath() {
        return mVideoPath;
    }

    public String getId() {
        return mVideoId;
    }
}
