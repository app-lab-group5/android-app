package com.group5.signlanguagelearning.community;

import com.group5.signlanguagelearning.BasePresenter;
import com.group5.signlanguagelearning.BaseView;

public interface CommunityContract {
    interface View extends BaseView<CommunityContract.Presenter> {

    }

    interface Presenter extends BasePresenter {

    }
}
