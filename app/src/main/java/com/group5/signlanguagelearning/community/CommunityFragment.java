package com.group5.signlanguagelearning.community;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.group5.signlanguagelearning.R;

public class CommunityFragment extends Fragment implements CommunityContract.View {
    CommunityContract.Presenter mCommunityPresenter;

    @Override
    public void setPresenter(CommunityContract.Presenter presenter) {
        this.mCommunityPresenter = presenter;
    }

    @Nullable
    @Override
    // called when this fragment is supposed to render its view
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.comm_fragment, container, false);
        // TODO add fragment logic
        return root;
    }
}
