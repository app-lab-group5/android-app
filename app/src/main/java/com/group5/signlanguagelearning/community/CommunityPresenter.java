package com.group5.signlanguagelearning.community;

public class CommunityPresenter implements CommunityContract.Presenter {
    private final CommunityContract.View mCommunityView;

    public CommunityPresenter(CommunityContract.View communityView) {
        this.mCommunityView = communityView;
    }

    @Override
    public void start() {

    }
}
