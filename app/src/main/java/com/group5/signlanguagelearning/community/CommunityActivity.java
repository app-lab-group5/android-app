package com.group5.signlanguagelearning.community;

import android.os.Bundle;
import android.support.v7.app.ActionBar;

import com.group5.signlanguagelearning.NavigationActivity;
import com.group5.signlanguagelearning.R;
import com.group5.signlanguagelearning.util.ActivityUtils;

public class CommunityActivity extends NavigationActivity {

    CommunityContract.Presenter mCommunityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar toolbar = getSupportActionBar();
        toolbar.setTitle(R.string.title_community);

        // set up the fragment
        CommunityFragment communityFragment =
                (CommunityFragment) getFragmentManager().findFragmentById(R.id.contentFrame);

        if (communityFragment == null){
            communityFragment = new CommunityFragment();
            ActivityUtils.addFragmentToActivity(getFragmentManager(),communityFragment,
                    R.id.contentFrame);
        }

        // presenter gets reference to view
        mCommunityPresenter = new CommunityPresenter(communityFragment);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_community;
    }

    @Override
    public int getNavigationMenuItemId() {
        return R.id.navigation_community;
    }

    @Override
    public int getActionBarId() {
        return R.id.toolbar;
    }
}
