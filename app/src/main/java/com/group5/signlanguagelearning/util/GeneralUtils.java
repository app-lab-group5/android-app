package com.group5.signlanguagelearning.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class GeneralUtils {

    public static String readStringFromFile(String filePath)
    {
        BufferedReader br = null;
        StringBuilder contentBuilder = new StringBuilder();
        try
        {
            br = new BufferedReader(new FileReader(filePath));
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null)
            {
                contentBuilder.append(sCurrentLine).append("\n");
            }
            br.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally {
            try {
                if(br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return contentBuilder.toString();
    }
}
