package com.group5.signlanguagelearning;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
