package com.group5.signlanguagelearning.dict.vocabPresentation;

import android.net.Uri;

import com.group5.signlanguagelearning.BasePresenter;
import com.group5.signlanguagelearning.BaseView;

public interface VocabPresentationContract {
    interface View extends BaseView<Presenter> {
        void showVideo(Uri videoUri);

        void showWord(String wordToShow);
    }

    interface Presenter extends BasePresenter {
        void openUpVideo();
    }
}
