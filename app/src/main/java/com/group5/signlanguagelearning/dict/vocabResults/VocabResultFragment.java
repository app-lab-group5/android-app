package com.group5.signlanguagelearning.dict.vocabResults;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.group5.signlanguagelearning.R;
import com.group5.signlanguagelearning.data.DataDummySource;
import com.group5.signlanguagelearning.data.VocabWord;
import com.group5.signlanguagelearning.dict.DictionaryActivity;
import com.group5.signlanguagelearning.dict.vocabPresentation.VocabPresentationFragment;
import com.group5.signlanguagelearning.dict.vocabPresentation.VocabPresentationPresenter;
import com.group5.signlanguagelearning.util.ActivityUtils;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class VocabResultFragment extends Fragment implements VocabResultContract.View{

    VocabResultContract.Presenter mResultPresenter;
    VocabWordAdapter mVocabWordAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO generalize this better to other fragments
        ((DictionaryActivity) getActivity()).showUpButton();
        mVocabWordAdapter = new VocabWordAdapter(
                new ArrayList<VocabWord>(0), mWordItemListener);
    }

    @Nullable
    @Override
    // called when this fragment is supposed to render its view
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.rslt_fragment, container, false);

        ListView listView = (ListView) root.findViewById(R.id.listView);
        // make this capable of updating its contents based on the adapter
        listView.setAdapter(mVocabWordAdapter);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        // TODO find out why this null check is necessary and if the reason
        // might cause us problems later down the road
        if(mResultPresenter != null) {
            mResultPresenter.start();
        }
    }

    @Override
    public void setPresenter(VocabResultContract.Presenter presenter) {
        this.mResultPresenter = presenter;
    }


    @Override
    public void showResults(List<VocabWord> words) {
            mVocabWordAdapter.setList(words);
    }

    @Override
    public void showVocabFragment(VocabWord toShow) {
        VocabPresentationFragment fragment = new VocabPresentationFragment();
        // hand over the data
        fragment.setPresenter(new VocabPresentationPresenter(DataDummySource.getInstance(getActivity().getApplicationContext()),
                toShow,fragment, getActivity().getApplicationContext()));

        ActivityUtils.replaceFragment(getFragmentManager(),fragment,
                R.id.contentFrame,true);
    }












    private static class VocabWordAdapter extends BaseAdapter {

        private List<VocabWord> mWords;
        private WordItemListener mWordItemListener;

        public VocabWordAdapter(List<VocabWord> words, WordItemListener mWordItemListener) {
            setList(words);
            this.mWordItemListener = mWordItemListener;
        }

        private void setList(List<VocabWord> words){
            mWords = checkNotNull(words);}

        @Override
        public int getCount() {
            return mWords.size();
        }

        @Override
        public VocabWord getItem(int position) {
            return mWords.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View listItem = convertView;
            // if this view has not been drawn yet,
            // build a new one from the XML layout spec
            if(listItem == null){
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                listItem = inflater.inflate(R.layout.word_item, parent, false);
            }
            // the word we want to put into this view
            final VocabWord word = getItem(position);

            TextView textView = (TextView) listItem.findViewById(R.id.wordText);
            // set the text
            textView.setText(word.getWord());

            // make this thing clickable
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mWordItemListener.onWordClick(word);
                }
            });

            return listItem;
        }
    }

    public interface WordItemListener {
        void onWordClick(VocabWord word);
    }

    WordItemListener mWordItemListener = new WordItemListener() {
        @Override
        public void onWordClick(VocabWord word) {
            mResultPresenter.openUpVocabWord(word);
        }
    };
}
