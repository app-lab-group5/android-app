package com.group5.signlanguagelearning.dict.vocabPresentation;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.group5.signlanguagelearning.R;

public class VocabPresentationFragment extends Fragment implements VocabPresentationContract.View {
    VocabPresentationContract.Presenter mPresenter;
    private VideoView mVideoView;
    private TextView mTextView;

    @Override
    public void setPresenter(VocabPresentationContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.vocab_fragment, container, false);
        mTextView = (TextView) root.findViewById(R.id.videoName);
        mVideoView = (VideoView) root.findViewById(R.id.videoView);
        MediaController controller = new MediaController(getActivity());
        controller.setAnchorView(mVideoView);

        mVideoView.setMediaController(controller);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        // TODO find out why this null check is necessary
        if(mPresenter != null) {
            mPresenter.start();
        }
    }

    @Override
    public void showVideo(Uri videoUri) {
        mVideoView.setVideoURI(videoUri);
        mVideoView.requestFocus();
        mVideoView.start();
    }

    @Override
    public void showWord(String wordToShow) {
        mTextView.setText(wordToShow);
    }
}
