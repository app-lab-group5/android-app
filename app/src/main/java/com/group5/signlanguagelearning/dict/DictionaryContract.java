package com.group5.signlanguagelearning.dict;

import android.support.annotation.NonNull;

import com.group5.signlanguagelearning.BasePresenter;
import com.group5.signlanguagelearning.BaseView;
import com.group5.signlanguagelearning.data.Category;

import java.util.List;

public interface DictionaryContract {

    interface View extends BaseView<Presenter>{
        // show item categories
        void showCategories(List<Category> categories);

        // show a list of words (in a new fragment)
        void showWordResults(List<String> wordIds);

        void showNoResultsFound();

    }

    interface Presenter extends BasePresenter{

        void filterCategory(@NonNull Category toShow);

        void searchForString(@NonNull String searchTerm);


    }
}
