package com.group5.signlanguagelearning.dict.vocabPresentation;

import android.content.Context;
import android.net.Uri;

import com.group5.signlanguagelearning.BuildConfig;
import com.group5.signlanguagelearning.R;
import com.group5.signlanguagelearning.data.Video;
import com.group5.signlanguagelearning.data.VideoProvider;
import com.group5.signlanguagelearning.data.VocabWord;

public class VocabPresentationPresenter implements VocabPresentationContract.Presenter {
    private VideoProvider provider;
    private VocabWord wordToShow;
    private VocabPresentationContract.View mPresView;
    private Context context;

    public VocabPresentationPresenter(VideoProvider provider, VocabWord wordToShow, VocabPresentationContract.View mPresView, Context context) {
        this.provider = provider;
        this.wordToShow = wordToShow;
        this.mPresView = mPresView;
        this.context = context;
    }

    @Override
    public void start() {
        this.openUpVideo();
    }

    public void setWordToShow(VocabWord wordToShow){
        this.wordToShow = wordToShow;
    }

    @Override
    public void openUpVideo() {
        this.mPresView.showWord(wordToShow.getWord());
        Video v = provider.getVideoForId(wordToShow.getVideoId());
        String packageName = BuildConfig.APPLICATION_ID;
        // get the resource id: TODO generalize this
        int id = context.getResources().getIdentifier(v.getVideoPath(),"raw",packageName);
        String UriPath = "android.resource://" + packageName + "/"
                + id;

        Uri videoUri = Uri.parse(UriPath);
        this.mPresView.showVideo(videoUri);
    }
}
