package com.group5.signlanguagelearning.dict;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.SearchView;
import android.widget.Toast;

import com.group5.signlanguagelearning.R;
import com.group5.signlanguagelearning.data.Category;
import com.group5.signlanguagelearning.data.DataDummySource;
import com.group5.signlanguagelearning.dict.vocabResults.VocabResultFragment;
import com.group5.signlanguagelearning.dict.vocabResults.VocabResultPresenter;
import com.group5.signlanguagelearning.util.ActivityUtils;

// weirdly enough, this functionality comes from guava
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;


public class DictionaryFragment extends Fragment implements DictionaryContract.View,
        FragmentManager.OnBackStackChangedListener{

    private DictionaryContract.Presenter mPresenter;
    private CategoryAdapter mCategoryAdapter;

    public DictionaryFragment() {
        super();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCategoryAdapter = new CategoryAdapter(
                new ArrayList<Category>(0),mCategoryListener);
    }

    @Nullable
    @Override
    // called when this fragment is supposed to render its view
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.dict_fragment, container, false);

        GridView categoryGrid = (GridView) root.findViewById(R.id.categoryGrid);
        // make this capable of updating its contents based on the adapter
        categoryGrid.setAdapter(mCategoryAdapter);
        SearchView mSearchView = root.findViewById(R.id.dictSearch);
        // hook this up
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null) {
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }
        return root;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        // TODO find out why this null check is necessary and if the reason
        // might cause us problems later down the road
        if(mPresenter != null) {
            mPresenter.start();
        }
    }

    @Override
    public void setPresenter(DictionaryContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void showCategories(List<Category> categories) {
        mCategoryAdapter.setList(categories);
    }

    @Override
    public void showWordResults(List<String> wordIds) {
        VocabResultFragment fragment = new VocabResultFragment();
        // hand over the data
        fragment.setPresenter(new VocabResultPresenter(fragment, DataDummySource.getInstance(getActivity().getApplicationContext()), wordIds));
        // replace the fragment
        ActivityUtils.replaceFragment(getFragmentManager(),fragment,
                R.id.contentFrame,true);
    }

    @Override
    public void showNoResultsFound() {
        String noResult = getResources().getString(R.string.no_result);
        Toast toast = Toast.makeText(getActivity().getApplicationContext(),noResult,Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onBackStackChanged() {
        // enable Up button only if there are entries on the backstack
        if(getActivity().getFragmentManager().getBackStackEntryCount() < 1) {
            ((DictionaryActivity) getActivity()).hideUpButton();
        }
    }

    private static class CategoryAdapter extends BaseAdapter{

        private List<Category> mCategories;
        private CategoryItemListener mCategoryItemListener;

        public CategoryAdapter(List<Category> categories, CategoryItemListener mCategoryItemListener) {
            setList(categories);
            this.mCategoryItemListener = mCategoryItemListener;
        }

        private void setList(List<Category> categories){mCategories = checkNotNull(categories);}

        @Override
        public int getCount() {
            return mCategories.size();
        }

        @Override
        public Category getItem(int position) {
            return mCategories.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View gridItem = convertView;
            // if this view has not been drawn yet,
            // build a new one from the XML layout spec
            if(gridItem == null){
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                gridItem = inflater.inflate(R.layout.category_item, parent, false);
            }
            // the category we want to put into this view
            final Category category = getItem(position);

            Button categoryButton = (Button) gridItem.findViewById(R.id.category_button);
            // set the text
            categoryButton.setText(category.getName());
            // set the drawable to the top location
            categoryButton.setCompoundDrawables(null,category.getIcon(),null,null);

            // make this thing clickable
            categoryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   mCategoryItemListener.onCategoryClick(category);
                }
            });

            return gridItem;
        }
    }

    public interface CategoryItemListener{
        void onCategoryClick(Category clickedCategory);
    }

    CategoryItemListener mCategoryListener = new CategoryItemListener() {
        @Override
        public void onCategoryClick(Category clickedCategory) {
            mPresenter.filterCategory(clickedCategory);
        }
    };
}
