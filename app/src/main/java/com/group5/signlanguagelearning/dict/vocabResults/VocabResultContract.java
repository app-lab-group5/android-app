package com.group5.signlanguagelearning.dict.vocabResults;

import com.group5.signlanguagelearning.BasePresenter;
import com.group5.signlanguagelearning.BaseView;
import com.group5.signlanguagelearning.data.VocabWord;

import java.util.List;

public interface VocabResultContract {
    interface View extends BaseView<VocabResultContract.Presenter> {

        void showResults(List<VocabWord> words);

        void showVocabFragment(VocabWord toShow);
    }

    interface Presenter extends BasePresenter {
        void openUpVocabWord(VocabWord word);
    }
}
