package com.group5.signlanguagelearning.dict;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;

import com.group5.signlanguagelearning.NavigationActivity;
import com.group5.signlanguagelearning.R;
import com.group5.signlanguagelearning.data.DataDummySource;
import com.group5.signlanguagelearning.util.ActivityUtils;

public class DictionaryActivity extends NavigationActivity{

    DictionaryPresenter mDictionaryPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar toolbar = getSupportActionBar();
        toolbar.setTitle(R.string.title_dictionary);

        // set up the fragment
        DictionaryFragment dictionaryFragment =
                (DictionaryFragment) getFragmentManager().findFragmentById(R.id.contentFrame);

        if (dictionaryFragment == null){
            dictionaryFragment = new DictionaryFragment();
            ActivityUtils.addFragmentToActivity(getFragmentManager(), dictionaryFragment,
                    R.id.contentFrame);
        }

        // presenter gets reference to view
        mDictionaryPresenter = new DictionaryPresenter(dictionaryFragment, DataDummySource.getInstance(this.getApplicationContext()),
                DataDummySource.getInstance(this.getApplicationContext()));

//        Intent intent = getIntent();
//
//        if(Intent.ACTION_SEARCH.equals(intent.getAction())){
//            String query = intent.getStringExtra(SearchManager.QUERY);
//            mDictionaryPresenter.searchForString(query);
//        }

        // TODO see if state loading necessary
    }

    @Override
    protected void onNewIntent(Intent intent) {
       // super.onNewIntent(intent);

        if(Intent.ACTION_SEARCH.equals(intent.getAction())){
            String query = intent.getStringExtra(SearchManager.QUERY);
            mDictionaryPresenter.searchForString(query);
        }

    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_dict;
    }

    @Override
    public int getNavigationMenuItemId() {
        return R.id.navigation_dictionary;
    }

    @Override
    public int getActionBarId() {
        return R.id.toolbar;
    }

    @Override
    // the logic is from: https://stackoverflow.com/a/32590524
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showUpButton() { getSupportActionBar().setDisplayHomeAsUpEnabled(true); }
    public void hideUpButton() { getSupportActionBar().setDisplayHomeAsUpEnabled(false); }
}
