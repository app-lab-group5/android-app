package com.group5.signlanguagelearning.dict.vocabResults;

import com.group5.signlanguagelearning.data.VocabWord;
import com.group5.signlanguagelearning.data.VocabWordProvider;

import java.util.List;

public class VocabResultPresenter implements VocabResultContract.Presenter {
    private final VocabResultContract.View mResultView;
    private VocabWordProvider mVocabWordProvider;
    private List<String> mIdsToShow;

    public VocabResultPresenter(VocabResultContract.View resultView,
                                VocabWordProvider mVocabWordProvider, List<String> idsToShow) {
        this.mResultView = resultView;
        this.mVocabWordProvider = mVocabWordProvider;
        this.mIdsToShow = idsToShow;
    }

    @Override
    public void start() {
        // load & show the stuff
        List<VocabWord> words = mVocabWordProvider.getVocabWordsForID(mIdsToShow);
        mResultView.showResults(words);
    }

    @Override
    public void openUpVocabWord(VocabWord word) {
        mResultView.showVocabFragment(word);
    }
}
