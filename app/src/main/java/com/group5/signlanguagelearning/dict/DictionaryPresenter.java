package com.group5.signlanguagelearning.dict;

import android.support.annotation.NonNull;

import com.group5.signlanguagelearning.data.Category;
import com.group5.signlanguagelearning.data.CategoryProvider;
import com.group5.signlanguagelearning.data.VocabWord;
import com.group5.signlanguagelearning.data.VocabWordProvider;

import java.util.List;

public class DictionaryPresenter implements DictionaryContract.Presenter{

    private final DictionaryContract.View mDictionaryView;
    private final CategoryProvider mCategoryProvider;
    private final VocabWordProvider mWordProvider;

    public DictionaryPresenter(DictionaryContract.View searchView, CategoryProvider categoryProvider,
                               VocabWordProvider wordProvider) {
        this.mDictionaryView = searchView;
        this.mDictionaryView.setPresenter(this);
        this.mCategoryProvider = categoryProvider;
        this.mWordProvider = wordProvider;
    }

    @Override
    public void start() {
        loadCategories();
    }

    private void loadCategories(){
        List<Category> mockData = mCategoryProvider.getCategories();
        mDictionaryView.showCategories(mockData);
    }

    @Override
    public void filterCategory(@NonNull Category toShow) {
        List<String> idResults = toShow.getIncludedWordIds();
        this.mDictionaryView.showWordResults(idResults);
    }

    @Override
    public void searchForString(@NonNull String searchTerm) {
        List<String> results = mWordProvider.getVocabWordIdsForWord(searchTerm);
        if(results.size() == 0){
            this.mDictionaryView.showNoResultsFound();
        }
        else {
            this.mDictionaryView.showWordResults(results);
        }
    }
}
